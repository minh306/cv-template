import React from 'react';

function App() {
  return (
    <div className="App">

      <div className="fixed-bg"></div>
      {/* <div className="fixed-bg-overlay"></div> */}

      <div id="pre_load" className="preloader-it">
        <div id="la_anim" className="la-anim-1"></div>
      </div>

      <div className="container-fluid">
        <div className="row">
          <div id="vertical_nav_wrap" className="col-lg-4 col-xs-12 pad-zero vertical-nav-wrap">

            <nav className="navbar navbar-default vertical-nav" role="navigation">
              <div className="navbar-header">
                <button type="button" className="btn btn-default navbar-toggle" data-toggle="collapse"
                  data-target="#navbar_collapse">
                  <span className="mask"></span>
                  <span className="btn-label">
                    <span className="pe-7s-menu"></span>
                  </span>
                </button>
              </div>
              <div className="collapse navbar-collapse" id="navbar_collapse">
                <ul className="nav navbar-nav">
                  <li><a data-scroll href="#main_content"></a><span>about</span></li>
                  <li><a data-scroll href="#services_sec"></a><span>services</span></li>
                  <li><a data-scroll href="#skill_sec"></a><span>skills</span></li>
                  {/* <li><a data-scroll href="#work_sec"></a><span>work</span></li> */}
                  <li><a data-scroll href="#edu_sec"></a><span>education</span></li>
                  <li><a data-scroll href="#exp_sec"></a><span>experience</span></li>
                  {/* <li><a data-scroll href="#client_sec"></a><span>clients</span></li> */}
                  {/* <li><a data-scroll href="#achivement_sec"></a><span>achivements</span></li> */}
                  <li><a data-scroll href="#contact_sec"></a><span>contact</span></li>
                </ul>
              </div>
            </nav>

          </div>

          <div className="col-lg-8 col-xs-12 pad-zero">

            <div id="main_content" className="content-block margin-top-150 margin-top-sm-70 margin-top-xs-50">
              <section id="intro_content_sec" className="col-lg-10 col-md-11 col-sm-11 center-div intro-content-wrap sec-pad">
                <div className="person-img margin-bottom-xs"></div>
                <h1>
                  Hey!<span id="typed"></span>
                </h1>
                <h6>about</h6>
                <p className="pad-bottom-35 wow fadeInUp" data-wow-duration=".6s">
                  Take an advantage of skills and experience to understand the knowledge for becoming a professional Front-end Developer. Besides that, bring the good quality product to customers.
							  </p>
                <p className="pad-bottom-35 wow fadeInUp" data-wow-duration=".6s">
                  I need an environment to develop myself and learn more about this field, where I can apply this knowledge I have learned to create products that are really useful for users, besides can enhance my knowledge. I want to be a valuable staff. I'm willing to work hard to achieve my goals.
                </p>
                <div className="per-signature wow fadeInUp" data-wow-duration=".4s">
                  <img className="img-responsive" src="img/signature.png" alt="signature" />
                </div>
              </section>

              <hr className="separater-hr" />

              <section id="services_sec" className="col-lg-10 col-md-11 col-sm-11 center-div services-icon-wrap  sec-pad-top sec-pad-bottom-xs">
                <h6>what i do</h6>
                <div className="row">
                  {/* <div className="col-sm-3 col-xs-6 margin-bottom-sm icon-wrap wow fadeInLeft" data-wow-duration=".4s" data-wow-delay="0s">
                    <span className="icon pe-7s-graph3"></span>
                    <span className="key-fact">
                      ANALYSIS
									</span>
                  </div> */}
                  <div className="col-sm-3 col-xs-6 margin-bottom-sm icon-wrap wow fadeInLeft" data-wow-duration=".4s" data-wow-delay=".1s">
                    <span className="icon pe-7s-browser"></span>
                    <span className="key-fact">
                      PROGRAMMING
									</span>
                  </div>
                  {/* <div className="col-sm-3 col-xs-6 margin-bottom-sm icon-wrap wow fadeInLeft" data-wow-duration=".4s" data-wow-delay=".2s">
                    <span className=" icon pe-7s-tools"></span>
                    <span className="key-fact">
                      TESTING
									</span>
                  </div> */}
                  {/* <div className="col-sm-3 col-xs-6 margin-bottom-sm icon-wrap wow fadeInLeft" data-wow-duration=".4s" data-wow-delay=".3s">
                    <span className="icon pe-7s-note2"></span>
                    <span className="key-fact">
                      DOCUMENTATION
									</span>
                  </div> */}
                </div>
              </section>

              <hr className="separater-hr" />

              <section id="skill_sec" className="col-lg-10 col-md-11 col-sm-11 center-div skills-wrap  sec-pad-top sec-pad-bottom-xs">
                <h6>technical skills</h6>
                <p className="wow fadeInUp" data-wow-duration=".4s">As a developer, I fled from a traditional path and explored my creativity, trough trying my hands on various techniques and software.</p>
                <div className="row margin-top-40">
                  <div className="col-sm-5 margin-bottom-sm">
                    <span className="progress-cat">HTML / CSS</span>
                    <div className="progress-bar-graph">
                      <div className="progress-bar-wrap">
                        <div className="bar-wrap">
                          <span data-width="70">
                            <strong><i>70</i>%</strong>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-offset-1 col-sm-5 margin-bottom-sm">
                    <span className="progress-cat">JAVASCRIPT</span>
                    <div className="progress-bar-graph">
                      <div className="progress-bar-wrap">
                        <div className="bar-wrap">
                          <span data-width="70">
                            <strong><i>70</i>%</strong>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-5 margin-bottom-sm">
                    <span className="progress-cat">REACT JS</span>
                    <div className="progress-bar-graph">
                      <div className="progress-bar-wrap">
                        <div className="bar-wrap">
                          <span data-width="70">
                            <strong><i>70</i>%</strong>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <div className="clearfix"></div> */}
                  <div className="col-sm-offset-1 col-sm-5 margin-bottom-sm">
                    <span className="progress-cat">NODE JS</span>
                    <div className="progress-bar-graph">
                      <div className="progress-bar-wrap">
                        <div className="bar-wrap">
                          <span data-width="50">
                            <strong><i>50</i>%</strong>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-5 margin-bottom-sm">
                    <span className="progress-cat">MONGO DB</span>
                    <div className="progress-bar-graph">
                      <div className="progress-bar-wrap">
                        <div className="bar-wrap">
                          <span data-width="40">
                            <strong><i>40</i>%</strong>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="clearfix"></div>
                  {/* <div className="col-sm-5 margin-bottom-sm">
                    <span className="progress-cat">JQUERY</span>
                    <div className="progress-bar-graph">
                      <div className="progress-bar-wrap">
                        <div className="bar-wrap">
                          <span data-width="86">
                            <strong><i>86</i>%</strong>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div> */}
                  {/* <div className="col-sm-offset-1 col-sm-5 margin-bottom-sm">
                    <span className="progress-cat">PHP</span>
                    <div className="progress-bar-graph">
                      <div className="progress-bar-wrap">
                        <div className="bar-wrap">
                          <div className="bar-wrap">
                            <span data-width="70">
                              <strong><i>70</i>%</strong>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> */}
                  {/* <div className="clearfix"></div> */}
                </div>
              </section>

              <hr className="separater-hr" />

              {/* <section id="work_sec" className="col-lg-10 col-md-11 col-sm-11 center-div skills-wrap  sec-pad">
                <h6>work</h6>

                <div id="portfolio-wrap" className="padding-sec-lg">
                  <ul id="portfolio" className="auto-construct  project-gallery" data-col="2">
                    <li className="item" data-src="img/mock1.jpg" data-sub-html="<h6>DESIGN LAB</h6><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>" >
                      <a href="">
                        <img className="img-responsive" src="img/mock1.jpg" alt="Image description" />
                        <span className="hover-cap">DESIGN LAB</span>
                      </a>
                    </li>
                    <li className="item" data-src="http://www.youtube.com/watch?v=Pq9yPrLWMyU" data-poster="img/mock2.jpg" data-sub-html="<h6>MODERN WORK</h6><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                      <a href="">
                        <img className="img-responsive" src="img/mock2.jpg" alt="Image description" />
                        <span className="hover-cap">MODERN WORK</span>
                      </a>
                    </li>
                    <li className="item" data-src="img/mock4.jpg" data-sub-html="<h6>UX PLATFORM</h6><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                      <a href="">
                        <img className="img-responsive" src="img/mock4.jpg" alt="Image description" />
                        <span className="hover-cap">UX PLATFORM</span>
                      </a>
                    </li>
                    <li className="item" data-src="http://vimeo.com/1084537" data-poster="img/mock3.jpg" data-sub-html="<h6>MONOBRANDING</h6><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                      <a href="">
                        <img className="img-responsive" src="img/mock3.jpg" alt="Image description" />
                        <span className="hover-cap">MONOBRANDING</span>
                      </a>
                    </li>

                    <li className="item" data-src="img/mock5.jpg" data-sub-html="<h6>FALL/WINTER</h6><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                      <a href="">
                        <img className="img-responsive" src="img/mock5.jpg" alt="Image description" />
                        <span className="hover-cap">FALL/WINTER</span>
                      </a>
                    </li>
                    <li className="item" data-html="#video1" data-poster="img/mock6.jpg" data-sub-html="<h6>FANTASTIC MAN</h6><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                      <a href="">
                        <img className="img-responsive" src="img/mock6.jpg" alt="Image description" />
                        <span className="hover-cap">FANTASTIC MAN</span>
                      </a>
                    </li>

                  </ul>
                  <div style={{ display: 'none' }} id="video1">
                    <video className="lg-video-object lg-html5 video-js vjs-default-skin" controls preload="none">
                      <source src="videos/video1.mp4" type="video/webm" />
                      <source src="videos/video1.webm" type="video/webm" />
                          Your browser does not support HTML5 video.
									</video>
                  </div>
                </div>
              </section> */}

              <hr className="separater-hr" />

              <section id="edu_sec" className="col-lg-10 col-md-11 col-sm-11 center-div education-wrap  sec-pad">
                <h6>education</h6>
                <p className="wow fadeInUp" data-wow-duration=".4s">
                  I spent 4 years studying at University of Transport and Communications Campus in Ho Chi Minh City, got my Bachelor degree in Software technology and started working. I think one should never stop learning and go through a process of acquiring knowledge.
							</p>
                <div className="qualfication-wrap">
                  <div className="qualfication first-row wow fadeInUp" data-wow-duration=".4s">
                    <div className="col-sm-2">
                      <span className="row-count"><span>1</span></span>
                    </div>
                    <div className="col-sm-2">
                      <span className="yr-pers vertical-align-pad">GPA</span>
                    </div>
                    <div className="col-sm-5">
                      <span className="insti vertical-align-pad">4 years</span>
                    </div>
                    <div className="col-sm-3">
                      <span className="design vertical-align-pad">
                        2.62
										  </span>
                    </div>
                  </div>
                  <div className="qualfication wow fadeInUp" data-wow-duration=".4s">
                    <div className="col-sm-2">
                      <span className="row-count"><span>2</span></span>
                    </div>
                    <div className="col-sm-2">
                      <span className="yr-pers vertical-align-pad">Award certificate</span>
                    </div>
                    <div className="col-sm-5">
                      <span className="insti vertical-align-pad">2017 - 2018</span>
                    </div>
                    <div className="col-sm-3">
                      <span className="design vertical-align-pad">
                        Fair
										  </span>
                    </div>
                  </div>
                  {/* <div className="qualfication wow fadeInUp" data-wow-duration=".4s">
                    <div className="col-sm-2">
                      <span className="row-count"><span>3</span></span>
                    </div>
                    <div className="col-sm-2">
                      <span className="yr-pers vertical-align-pad">2009-2012</span>
                    </div>
                    <div className="col-sm-5">
                      <span className="insti vertical-align-pad">Queensglen Public University, California</span>
                    </div>
                    <div className="col-sm-3">
                      <span className="design vertical-align-pad">
                        Web Design Diploma
										</span>
                    </div>
                  </div> */}

                </div>
              </section>

              <hr className="separater-hr" />

              <section id="exp_sec" className="col-lg-10 col-md-11 col-sm-11 center-div experience-wrap  sec-pad">
                <h6>experience</h6>
                <div className="exp-timeline-wrap">
                  <div className="row exp-timeline timeline-active">
                    <div className="col-xs-2">
                      <div className="timeline-st"></div>
                    </div>
                    <div className="col-xs-10 exp-content-wrap">
                      <div className="row">
                        <div className="wow fadeInRight" data-wow-duration=".4s" data-wow-delay="0s">
                          <div className="col-sm-3 duration"><span>oct 2019 - present</span></div>
                          <div className="col-sm-9">
                            <span className="job-desn">Frontend Developer</span>
                            <span className="job-loc">Asset Holdings, District 2</span>
                            <p className="job-summary p-small">Main Job</p>
                            <ul style={{ listStyle: 'circle' }}>
                              <li>Assign suitable jobs for members</li>
                              <li>Create an interface according to the design</li>
                              <li>Do the functions required by the superiors (Client and Admin page)</li>
                              <li>Write API for Frontend</li>
                              <li>Fix bug</li>
                              <li>Work with Antd framework, React google map, Redux Thunk</li>
                            </ul>
                            <p>Achievement and gained skill</p>
                            <ul style={{ listStyle: 'circle' }}>
                              <li>Improve self-study and research</li>
                              <li>Complete the assigned tasks on time</li>
                              <li>Withstand the pressure of work</li>
                              <li>Increased problem solving</li>
                              <li>Approach backend knowledge</li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <div className="row exp-timeline">
                    <div className="col-xs-2">
                      <div className="timeline-st"></div>
                    </div>
                    <div className="col-xs-10 exp-content-wrap">
                      <div className="row">
                        <div className="wow fadeInRight" data-wow-duration=".4s" data-wow-delay="0.2s">
                          <div className="col-sm-3 duration"><span>Jul 2013 — Mar 2014</span></div>
                          <div className="col-sm-9">
                            <span className="job-desn">Visual Communication Designer</span>
                            <span className="job-loc">Dropzone, NYC
	</span>
                            <p className="job-summary p-small">Being in a very well structured company, with clear and defined work procedures, helped me grow both personally and professionally, as well as enjoying a young and international working environment. I had the opportunity to redefine the way I work in a more intelligent way.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> */}
                  {/* <div className="row exp-timeline">
                    <div className="col-xs-2">
                      <div className="timeline-st"></div>
                    </div>
                    <div className="col-xs-10 exp-content-wrap">
                      <div className="row">
                        <div className="wow fadeInRight" data-wow-duration=".4s" data-wow-delay="0.4s">
                          <div className="col-sm-3 duration"><span>Jan 2013 – Jul 2013</span></div>
                          <div className="col-sm-9">
                            <span className="job-desn">Web Publishing
	</span>
                            <span className="job-loc">Daisystreet, NYC
	</span>
                            <p className="job-summary p-small">Being the first design agency in my career and being one among the best in New York, I've learned to be proficient in a fast-paced work environment.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> */}
                  {/* <div className="row exp-timeline">
                    <div className="col-xs-2">
                      <div className="timeline-st"></div>
                    </div>
                    <div className="col-xs-10 exp-content-wrap">
                      <div className="row">
                        <div className="wow fadeInRight" data-wow-duration=".4s" data-wow-delay="0.6s">
                          <div className="col-sm-3 duration"><span>Oct 2011 – Jan 2013</span></div>
                          <div className="col-sm-9">
                            <span className="job-desn">Intern Developer
	</span>
                            <span className="job-loc">Oxwalk Ducks, California
	</span>
                            <p className="job-summary p-small">Create frameworks to guide critical thinking, to streamline the ideation process and to visualize complex concepts. Uses various graphic approaches to make complex ideas more tangible.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> */}
                </div>
              </section>

              <hr className="separater-hr" />

              {/* <section id="client_sec" className="col-lg-10 col-md-11 col-sm-11 center-div client-wrap  sec-pad wow bounceIn" data-wow-duration=".4s">
                <h6>clients</h6>
                <div className="client-carousel">
                  <img src="img/client1.png" alt="client" />
                  <img src="img/client2.png" alt="client" />
                  <img src="img/client3.png" alt="client" />
                  <img src="img/client4.png" alt="client" />
                  <img src="img/client5.png" alt="client" />
                  <img src="img/client2.png" alt="client" />
                </div>
              </section> */}

              {/* <hr className="separater-hr" /> */}

              {/* <section id="achivement_sec" className="col-lg-10 col-md-11 col-sm-11 center-div achivement-wrap  sec-pad-top sec-pad-bottom-sm">
                <h6>achivements</h6>
                <p className="wow fadeInUp" data-wow-duration=".4s">It always seems impossible until its done. Participated in more than 100 contests run online and run by government. I got first prize for Logo Design done for a Fashion Brand in 2015, the ceremony held in Germany.</p>
                <div className="achivement-list-wrap margin-top-40">
                  <ul className="row">
                    <li className="col-sm-6">
                      <ul>
                        <li className="margin-bottom-xs wow fadeInUp" data-wow-duration=".4s" data-wow-delay="0s">
                          <span className="dash-pointer">-</span>
                          <span className="achive-pts">First Price for Logo Design in Berman Design Award</span>
                        </li>
                        <li className="margin-bottom-xs wow fadeInUp" data-wow-duration=".4s" data-wow-delay=".2s"><span className="dash-pointer">-</span><span className="achive-pts">Honorable Mention at International Design Awards</span></li>
                        <li className="margin-bottom-xs wow fadeInUp" data-wow-duration=".4s" data-wow-delay=".4s"><span className="dash-pointer">-</span><span className="achive-pts">AICB New York, USA</span></li>
                        <li className="margin-bottom-xs wow fadeInUp" data-wow-duration=".4s" data-wow-delay=".6s"><span className="dash-pointer">-</span><span className="achive-pts">International CNDY Awards New York, USA</span></li>
                      </ul>
                    </li>
                    <li className="col-sm-6">
                      <ul>
                        <li className="margin-bottom-xs wow fadeInUp" data-wow-duration=".4s" data-wow-delay="0s"><span className="dash-pointer">-</span><span className="achive-pts">Drix Club des Directeurs Artistiques Paris, France</span></li>
                        <li className="margin-bottom-xs wow fadeInUp" data-wow-duration=".4s" data-wow-delay="0.2s"><span className="dash-pointer">-</span><span className="achive-pts">A&AD Awards (Yellow & Black Pencils) London, UK</span></li>
                        <li className="margin-bottom-xs wow fadeInUp" data-wow-duration=".4s" data-wow-delay=".4s"><span className="dash-pointer">-</span><span className="achive-pts">Global Dffie Awards New York, USA</span></li>
                        <li className="margin-bottom-xs wow fadeInUp" data-wow-duration=".6s" data-wow-delay=".6s"><span className="dash-pointer">-</span><span className="achive-pts">The Intercontinental Advertising Award (The Cup) (ICAA) Spain</span></li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </section> */}

              {/* <hr className="separater-hr" /> */}

              <section id="contact_sec" className="col-lg-10 col-md-11 col-sm-11 center-div contact-wrap  sec-pad-top sec-pad-bottom-sm">
                <h6>contact</h6>
                <div data-ng-controller="ContactController" className="align-center wow fadeInUp" data-wow-duration=".4s">
                  <h3>Hello <span data-ng-bind="formData.inputName||'friend'"></span>, I'd love to hear from you.</h3>
                  {/* <div>
                    <form data-ng-submit="submit(contactform, $event)" name="contactform" method="post" className="row form-horizontal" role="form">
                      <div className="form-group input--hoshi col-sm-6" data-ng-className="{ 'has-error': contactform.inputName.$invalid && submitted }">
                        <div className="input-wrap">
                          <input autocomplete="off" data-ng-model="formData.inputName" type="text" className="form-control input__field input input__field--hoshi" id="inputName" name="inputName" placeholder="Name" required />
                          <label className="input__label input__label input__label--hoshi input__label--hoshi-color-1 input__label--hoshi input__label--hoshi-color-1" ></label>
                        </div>
                      </div>
                      <div className="form-group  input--hoshi col-sm-6" data-ng-className="{ 'has-error': contactform.inputEmail.$invalid && submitted }">
                        <div className="input-wrap">
                          <input autocomplete="off" data-ng-model="formData.inputEmail" type="email" className="form-control input input__field input__field--hoshi" placeholder="Email" id="inputEmail" name="inputEmail" required />
                          <label className="input__label input__label--hoshi input__label--hoshi-color-1"></label>
                        </div>
                      </div>
                      <div className="form-group  input--hoshi col-sm-12" data-ng-className="{ 'has-error': contactform.inputMessage.$invalid && submitted }">
                        <div className="input-wrap">
                          <textarea data-ng-model="formData.inputMessage" className="form-control input input__field input__field--hoshi" rows="4" id="inputMessage" name="inputMessage" placeholder="message" required></textarea>
                          <label className="input__label input__label--hoshi input__label--hoshi-color-1"></label>
                        </div>
                      </div>
                      <div className="form-group col-sm-12">
                        <div className="align-center">
                          <button type="submit" className="btn btn-default" data-ng-disabled="submitButtonDisabled">
                            <span className="mask"></span>
                            <span>send message</span>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div> */}
                </div>

                {/* <div className="email-direct">
                  <p className="wow fadeInRight" data-wow-duration=".4s">Or simply email me at <a href="mailto:xyz@example.com">xyz@example.com</a></p>
                </div> */}
              </section>

              <hr className="separater-hr" />

              <footer className="col-lg-10 col-md-11 col-sm-11 center-div contact-wrap  sec-pad">
                <div className="row">
                  <div className="col-sm-6 available-wrap">
                    <span className="available-pointer"></span>
                    <span className="available-tag">Phone: 0792452548</span>
                  </div>
                  <div className="col-sm-6">
                    <div className="social-icons-wrap float-right">
                      <ul className="social-icons float-right">
                        <li>
                          <a href="https://www.facebook.com/minh.hoang.3006" className="social-icon">
                            <span className="fa" data-hover="&#xf09a;">&#xf09a;</span>
                          </a>
                        </li>
                        <li>
                          <a href="https://zalo.me/0792452548" className="social-icon">
                            <span className="fa" data-hover="&#xf099;"></span>
                            <img src={require("./zalo.png")} style={{ width: '100%' }}></img>
                          </a>
                        </li>
                        {/* <li>
                          <a href="#" className="social-icon">
                            <span className="fa" data-hover="&#xf16e;">&#xf16e;</span>
                          </a>
                        </li>
                        <li>
                          <a href="#" className="social-icon">
                            <span className="fa" data-hover="&#xf16d;">&#xf16d;</span>
                          </a>
                        </li> */}
                      </ul>
                    </div>
                    <div className="goto-top text-right float-right">
                      <a data-scroll href="#main_content">
                        <span data-hover="top">
                          top
											</span>
                      </a>
                    </div>
                    <div className="copywrite-wrap text-right float-right">
                      <p className="copywrite">Fabricated by <b>Hencework &copy; 2017.</b> Updated <b>June, 2017.</b></p>
                    </div>
                  </div>
                  <div className="col-sm-6 available-wrap">
                    <span className="available-tag" style={{ marginRight: 10 }}>Email: hoangminh30696@gmail.com</span>
                    <span className="available-pointer"></span>
                  </div>
                  
                </div>
              </footer>

            </div>

          </div>
        </div>
      </div>

    </div>
  );
}

export default App;
