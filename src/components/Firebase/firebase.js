import app from 'firebase/app';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyB2Qt02XQ_r8IaUlv7Y6VHS-JhB8epTE-0",
  authDomain: "cv-template-b5d7c.firebaseapp.com",
  databaseURL: "https://cv-template-b5d7c.firebaseio.com",
  projectId: "cv-template-b5d7c",
  storageBucket: "cv-template-b5d7c.appspot.com",
  messagingSenderId: "875708678671",
  appId: "1:875708678671:web:98e6980718d8020f99597a",
  measurementId: "G-796M5PK7TQ"
};

class Firebase {
  constructor() {
    app.initializeApp(firebaseConfig);
    this.auth = app.auth();
  }
}
 
export default Firebase;